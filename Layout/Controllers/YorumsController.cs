﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Layout.Models;

namespace Layout.Controllers
{
    public class YorumsController : Controller
    {
        private OyunSiteEntities db = new OyunSiteEntities();

        // GET: Yorums
        public ActionResult Index()
        {
            var yorums = db.Yorums.Include(y => y.Kullanici).Include(y => y.Oyunlar);
            return View(yorums.ToList());
        }

        // GET: Yorums/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yorum yorum = db.Yorums.Find(id);
            if (yorum == null)
            {
                return HttpNotFound();
            }
            return View(yorum);
        }

        // GET: Yorums/Create
        public ActionResult Create()
        {
            ViewBag.KullaniciID = new SelectList(db.Kullanicis, "KullaniciID", "KullaniciAd");
            ViewBag.OyunID = new SelectList(db.Oyunlars, "OyunID", "OyunAd");
            return View();
        }

        // POST: Yorums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "YorumID,YorumIcerik,OyunID,KullaniciID")] Yorum yorum)
        {
            if (ModelState.IsValid)
            {
                db.Yorums.Add(yorum);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KullaniciID = new SelectList(db.Kullanicis, "KullaniciID", "KullaniciAd", yorum.KullaniciID);
            ViewBag.OyunID = new SelectList(db.Oyunlars, "OyunID", "OyunAd", yorum.OyunID);
            return View(yorum);
        }

        // GET: Yorums/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yorum yorum = db.Yorums.Find(id);
            if (yorum == null)
            {
                return HttpNotFound();
            }
            ViewBag.KullaniciID = new SelectList(db.Kullanicis, "KullaniciID", "KullaniciAd", yorum.KullaniciID);
            ViewBag.OyunID = new SelectList(db.Oyunlars, "OyunID", "OyunDili", yorum.OyunID);
            return View(yorum);
        }

        // POST: Yorums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "YorumID,YorumIcerik,OyunID,KullaniciID")] Yorum yorum)
        {
            if (ModelState.IsValid)
            {
                db.Entry(yorum).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KullaniciID = new SelectList(db.Kullanicis, "KullaniciID", "KullaniciAd", yorum.KullaniciID);
            ViewBag.OyunID = new SelectList(db.Oyunlars, "OyunID", "OyunDili", yorum.OyunID);
            return View(yorum);
        }

        // GET: Yorums/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yorum yorum = db.Yorums.Find(id);
            if (yorum == null)
            {
                return HttpNotFound();
            }
            return View(yorum);
        }

        // POST: Yorums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Yorum yorum = db.Yorums.Find(id);
            db.Yorums.Remove(yorum);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
