﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Layout.Models;

namespace Layout.Controllers
{
    public class OyunlarsController : Controller
    {
        private OyunSiteEntities db = new OyunSiteEntities();

        
        public ActionResult Index(String SearchString)
        {
           var oyunara = from u in db.Oyunlars
                         join b in db.Kategoris on u.kategoriID equals b.kategoriID
                         select u;
                 if (!string.IsNullOrEmpty(SearchString))
        {
               oyunara = from u in db.Oyunlars
                         join b in db.Kategoris on u.kategoriID equals b.kategoriID
                         where (b.KategoriAd.ToUpper().Contains(SearchString.ToUpper()))
                    select u;
         }
           return View(oyunara);
        }

      
            public ActionResult Index1()
            {
            var kategori = from u in db.Oyunlars
                           join b in db.Kategoris on u.kategoriID equals b.kategoriID
                           where b.KategoriAd.Contains("yar")
                           select u;
                           
            return View(kategori);
            }

        public ActionResult Index2()
        {
            var kategori = from u in db.Oyunlars
                           join b in db.Kategoris on u.kategoriID equals b.kategoriID
                           where b.KategoriAd.Contains("spor")
                           select u;

            return View(kategori);
        }

        public ActionResult Index3()
        {
            var kategori = from u in db.Oyunlars
                           join b in db.Kategoris on u.kategoriID equals b.kategoriID
                           where b.KategoriAd.Contains("aksiyon")
                           select u;

            return View(kategori);
        }

        public ActionResult Index4()
        {
            var kategori = from u in db.Oyunlars
                           join b in db.Kategoris on u.kategoriID equals b.kategoriID
                           where b.KategoriAd.Contains("macera")
                           select u;

            return View(kategori);
        }



        // GET: Oyunlars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyunlar oyunlar = db.Oyunlars.Find(id);
            if (oyunlar == null)
            {
                return HttpNotFound();
            }
            return View(oyunlar);
        }

        public ActionResult Details1(int id = 17)
        {
            Oyunlar oyunlar = db.Oyunlars.Find(id);
            return View(oyunlar);


        }
        public ActionResult Details2(int id = 16)
        {
            Oyunlar oyunlar = db.Oyunlars.Find(id);
            return View(oyunlar);


        }
        public ActionResult Details3(int id = 14)
        {
            Oyunlar oyunlar = db.Oyunlars.Find(id);
            return View(oyunlar);


        }
        public ActionResult Details4(int id = 15)
        {
            Oyunlar oyunlar = db.Oyunlars.Find(id);
            return View(oyunlar);


        }
        public ActionResult Details5(int id = 13)
        {
            Oyunlar oyunlar = db.Oyunlars.Find(id);
            return View(oyunlar);
        }



        // GET: Oyunlars/Create
        public ActionResult Create()
        {
            ViewBag.kategoriID = new SelectList(db.Kategoris, "kategoriID", "KategoriAd");
            return View();
        }

        // POST: Oyunlars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OyunID,OyunDili,PlatformAd,SistemGereksinimleri,OyunUcreti,OyunHikaye,OyunAd,kategoriID")] Oyunlar oyunlar)
        {
            if (ModelState.IsValid)
            {
                db.Oyunlars.Add(oyunlar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.kategoriID = new SelectList(db.Kategoris, "kategoriID", "KategoriAd", oyunlar.kategoriID);
            return View(oyunlar);
        }

        // GET: Oyunlars/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyunlar oyunlar = db.Oyunlars.Find(id);
            if (oyunlar == null)
            {
                return HttpNotFound();
            }
            ViewBag.kategoriID = new SelectList(db.Kategoris, "kategoriID", "KategoriAd", oyunlar.kategoriID);
            return View(oyunlar);
        }

        // POST: Oyunlars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OyunID,OyunDili,PlatformAd,SistemGereksinimleri,OyunUcreti,OyunHikaye,OyunAd,kategoriID")] Oyunlar oyunlar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oyunlar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.kategoriID = new SelectList(db.Kategoris, "kategoriID", "KategoriAd", oyunlar.kategoriID);
            return View(oyunlar);
        }

        // GET: Oyunlars/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyunlar oyunlar = db.Oyunlars.Find(id);
            if (oyunlar == null)
            {
                return HttpNotFound();
            }
            return View(oyunlar);
        }

        // POST: Oyunlars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oyunlar oyunlar = db.Oyunlars.Find(id);
            db.Oyunlars.Remove(oyunlar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
